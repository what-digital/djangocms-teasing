"""
This file demonstrates two different styles of tests (one doctest and one
unittest). These will both pass when you run "manage.py test".

Replace these with more appropriate tests for your application.
"""

from datetime import timedelta

from cms.api import create_page
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.test import TestCase
from django.utils.timezone import now

from djangocms_teasing.models import Zone, Placement, TestTeaser, PagePool


class TestBase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('test', 'test', 'test')
        self.user.is_staff = True
        self.user.save()

        self.teaser = TestTeaser(is_active=True)
        self.zone = Zone(name="test", number_of_slots=1, site=Site.objects.get(pk=1))
        self.zone.save()
        self.zone.content_types.add(ContentType.objects.get(name="test teaser"))

        self.page = create_page("test", "test.html", "en", created_by=self.user, published=True)


class ModelTests(TestBase):
    def tearDown(self):
        for p in Placement.objects.all():
            p.delete()

        for t in TestTeaser.objects.all():
            t.delete()

    def test_create_test_teaser(self):
        self.teaser.save()
        self.assertEqual(len(TestTeaser.objects.all()), 1)

    def test_create_zone(self):
        self.zone.save()
        self.zone.content_types.add(ContentType.objects.get(name="test teaser"))
        self.teaser.save()
        Placement(zone=self.zone, content_type=TestTeaser, content_object=self.teaser, object_id=self.teaser.id).save()
        self.assertEqual(len(Zone.objects.filter(name="test")), 1)

    def test_placement_must_have_a_zone(self):
        self.zone.number_of_slots = 3
        self.zone.save()

        teaser = TestTeaser(is_active=True, title="test")
        teaser.save()

        Placement(content_type=TestTeaser,
                  content_object=teaser,
                  object_id=teaser.id,
                  page=self.page).save()

        self.assertEqual(len(self.zone.get_teaserset(self.page)), 0)

    def test_teaser_with_too_high_fixed_slot(self):
        self.zone.number_of_slots = 3
        self.zone.save()

        teaser = TestTeaser(is_active=True, title="test")
        teaser.save()

        Placement(content_type=TestTeaser,
                  content_object=teaser,
                  object_id=teaser.id,
                  zone=self.zone,
                  page=self.page,
                  fixed_slot=23).save()

        self.assertEqual(len(self.zone.get_teaserset(self.page)), 0)

    def test_publish_until_in_a_minute(self):
        self.zone.save()
        self.zone.content_types.add(ContentType.objects.get(name="test teaser"))
        self.teaser.save()

        Placement(zone=self.zone,
                  content_type=TestTeaser,
                  content_object=self.teaser,
                  object_id=self.teaser.id,
                  publish_from=now(),
                  publish_until=now() + timedelta(minutes=1)).save()

        self.assertEqual(len(TestTeaser.active.all()), 1)

    def test_publish_from_until_a_minute_before(self):
        self.zone.save()
        self.zone.content_types.add(ContentType.objects.get(name="test teaser"))
        self.teaser.save()

        Placement(zone=self.zone,
                  content_type=TestTeaser,
                  content_object=self.teaser,
                  object_id=self.teaser.id,
                  publish_from=now() - timedelta(days=14),
                  publish_until=now() - timedelta(minutes=1)).save()

        self.assertEqual(len(TestTeaser.active.all()), 0)

    def test_publish_from_with_open_end(self):
        self.zone.save()
        self.zone.content_types.add(ContentType.objects.get(name="test teaser"))
        self.teaser.save()

        Placement(zone=self.zone,
                  content_type=TestTeaser,
                  content_object=self.teaser,
                  object_id=self.teaser.id,
                  publish_from=now()).save()

        self.assertEqual(len(TestTeaser.active.all()), 1)

    def test_publish_from_with_future_date(self):
        self.zone.save()
        self.zone.content_types.add(ContentType.objects.get(name="test teaser"))
        self.teaser.save()

        Placement(zone=self.zone,
                  content_type=TestTeaser,
                  content_object=self.teaser,
                  object_id=self.teaser.id,
                  publish_from=now() + timedelta(days=3)).save()

        self.assertEqual(len(TestTeaser.active.all()), 0)

    def test_publish_until_tomorrow_without_from(self):
        self.zone.save()
        self.zone.content_types.add(ContentType.objects.get(name="test teaser"))
        self.teaser.save()

        Placement(zone=self.zone,
                  content_type=TestTeaser,
                  content_object=self.teaser,
                  object_id=self.teaser.id,
                  publish_until=now() + timedelta(days=1)).save()

        self.assertEqual(len(TestTeaser.active.all()), 1)

    def test_publish_until_in_a_minute_without_from(self):
        self.zone.save()
        self.zone.content_types.add(ContentType.objects.get(name="test teaser"))
        self.teaser.save()

        Placement(zone=self.zone,
                  content_type=TestTeaser,
                  content_object=self.teaser,
                  object_id=self.teaser.id,
                  publish_until=now() + timedelta(minutes=1)).save()

        self.assertEqual(len(TestTeaser.active.all()), 1)

    def test_publish_until_a_minute_before_without_from(self):
        self.zone.save()
        self.zone.content_types.add(ContentType.objects.get(name="test teaser"))
        self.teaser.save()

        Placement(zone=self.zone,
                  content_type=TestTeaser,
                  content_object=self.teaser,
                  object_id=self.teaser.id,
                  publish_until=now() - timedelta(minutes=1)).save()

        self.assertEqual(len(TestTeaser.active.all()), 0)

    def test_publish_until_yesterday_without_from(self):
        self.zone.save()
        self.zone.content_types.add(ContentType.objects.get(name="test teaser"))
        self.teaser.save()

        Placement(zone=self.zone,
                  content_type=TestTeaser,
                  content_object=self.teaser,
                  object_id=self.teaser.id,
                  publish_until=now() - timedelta(days=1)).save()

        self.assertEqual(len(TestTeaser.active.all()), 0)

    def test_no_duplicates_in_zone_with_not_enough_teasers(self):
        self.zone.number_of_slots = 10
        self.zone.save()

        for i in range(0, 5):
            teaser = TestTeaser(is_active=True, title="test" + str(i))
            teaser.save()
            Placement(content_type=TestTeaser, content_object=teaser, object_id=teaser.id, zone=self.zone).save()

        teaserset = self.zone.get_teaserset(self.page)
        self.assertEqual(len(teaserset), 5)

    def test_teaser_inheritance(self):
        self.zone.number_of_slots = 3
        self.zone.save()

        teaser = TestTeaser(is_active=True, title="test")
        teaser.save()

        p = Placement(content_type=TestTeaser,
                      content_object=teaser,
                      object_id=teaser.id,
                      zone=self.zone,
                      page=self.page)
        p.save()

        child_page = create_page("test", "test.html", "en", created_by=self.user, published=True, parent=self.page)
        self.assertEqual(len(self.zone.get_teaserset(child_page)), 0)

        p.delete()

        p = Placement(content_type=TestTeaser,
                      content_object=teaser,
                      object_id=teaser.id,
                      zone=self.zone,
                      page=self.page,
                      inheritance=True)
        p.save()

        self.assertEqual(len(self.zone.get_teaserset(child_page)), 1)

    def test_no_duplicate_teaser_through_inheritance(self):
        self.zone.number_of_slots = 3
        self.zone.save()

        teaser = TestTeaser(is_active=True, title="test")
        teaser.save()

        child_page = create_page("test", "test.html", "en", created_by=self.user, published=True, parent=self.page)

        Placement(content_type=TestTeaser,
                  content_object=teaser,
                  object_id=teaser.id,
                  zone=self.zone,
                  page=self.page,
                  fixed_slot=1,
                  inheritance=True).save()

        Placement(content_type=TestTeaser,
                  content_object=teaser,
                  object_id=teaser.id,
                  zone=self.zone,
                  page=child_page,
                  fixed_slot=2,
                  inheritance=True)

        self.assertEqual(len(self.zone.get_teaserset(child_page)), 1)

    def test_inherit_teaser_and_fix_page_teaser(self):
        self.zone.number_of_slots = 3
        self.zone.save()

        teaser = TestTeaser(is_active=True, title="test")
        teaser.save()

        teaser2 = TestTeaser(is_active=True, title="test2")
        teaser2.save()

        child_page = create_page("test", "test.html", "en", created_by=self.user, published=True, parent=self.page)

        Placement(content_type=TestTeaser,
                  content_object=teaser,
                  object_id=teaser.id,
                  zone=self.zone,
                  page=self.page,
                  fixed_slot=1,
                  inheritance=True).save()

        Placement(content_type=TestTeaser,
                  content_object=teaser,
                  object_id=teaser.id,
                  zone=self.zone,
                  page=child_page,
                  fixed_slot=2,
                  inheritance=True)

        self.assertEqual(len(self.zone.get_teaserset(child_page)), 1)

    def test_page_pool(self):
        self.zone.number_of_slots = 3
        self.zone.save()

        pool = PagePool(name="test")
        pool.save()
        pool.pages.add(self.page)
        pool.save()

        teaser = TestTeaser(is_active=True, title="test")
        teaser.save()

        Placement(content_type=TestTeaser,
                  content_object=teaser,
                  object_id=teaser.id,
                  zone=self.zone,
                  page_pool=pool).save()

        self.assertEqual(len(self.zone.get_teaserset(self.page)), 1)

    def test_fix_page_teaser_and_page_pool(self):
        self.zone.number_of_slots = 3
        self.zone.save()

        pool = PagePool(name="test")
        pool.save()
        pool.pages.add(self.page)
        pool.save()

        teaser = TestTeaser(is_active=True, title="test")
        teaser.save()

        Placement(content_type=TestTeaser,
                  content_object=teaser,
                  object_id=teaser.id,
                  zone=self.zone,
                  page_pool=pool).save()

        Placement(content_type=TestTeaser,
                  content_object=teaser,
                  object_id=teaser.id,
                  fixed_slot=1,
                  zone=self.zone,
                  page=self.page).save()

        self.assertEqual(len(self.zone.get_teaserset(self.page)), 1)

    def test_no_teaser_on_page_linking_to_that_page(self):
        self.zone.number_of_slots = 3
        self.zone.save()

        self.assertEqual(len(self.zone.get_teaserset(self.page)), 0)
        teaser = TestTeaser(is_active=True, title="test", page=self.page)
        teaser.save()
        Placement(content_type=TestTeaser,
                  content_object=teaser,
                  object_id=teaser.id,
                  zone=self.zone,
                  page=self.page).save()

        self.assertEqual(len(self.zone.get_teaserset(self.page)), 0)
