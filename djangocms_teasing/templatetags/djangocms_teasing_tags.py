from classytags.arguments import Argument
from classytags.core import Options
from classytags.helpers import InclusionTag
from cms.utils.page_resolver import get_page_from_request
from django import template
from django.contrib.sites.shortcuts import get_current_site

from djangocms_teasing.models import Zone

register = template.Library()


class RenderTeasingZone(InclusionTag):
    name = 'render_teasingzone'
    template = 'djangocms_teasing/dummy.html'

    options = Options(
        Argument('name', required=True),
        Argument('template_name', required=True),
    )

    def get_context(self, context, name, template_name='djangocms_teasing/dummy.html'):
        request = context['request']

        # Get teasing zone or render empty template
        try:
            zone = Zone.objects.get(site=get_current_site(request), name=name)
        except Zone.DoesNotExist:
            return {'template': 'djangocms_teasing/empty.html'}

        context.update({
            'template': template_name,
            'teaser_list': zone.get_teaserset(page=get_page_from_request(request))
        })
        return context


register.tag(RenderTeasingZone)
