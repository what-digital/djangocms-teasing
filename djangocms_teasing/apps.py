from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DjangoCMSTeasingConfig(AppConfig):
    name = 'djangocms_teasing'
    verbose_name = _("django CMS Teasing")
