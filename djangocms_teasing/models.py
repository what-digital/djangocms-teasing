import random

import cms.models.pagemodel
from cms.models import Page
from cms.models.fields import PageField
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.query_utils import Q
from django.forms import ModelChoiceField, ModelMultipleChoiceField
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _


# teaser rules (select as long as there are free slots):
# - fix page teasers
# - page teasers
# - inherit fix page teaser
# - inherit non-fix page teaser
# - global fix teasers
# - global non-fix teasers


class PageModelChoiceField(ModelChoiceField):
    def __init__(self, display_site_name=True, *args, **kwargs):
        self.display_site_name = display_site_name
        super(PageModelChoiceField, self).__init__(*args, **kwargs)

    def label_from_instance(self, page):
        # Django-cms >= 3.1 uses treebeard instead uf mptt. Thus, the page object
        # doesn't have level attribute anymore. The following will work for both.
        page_depth = getattr(page, 'level', page.depth)
        if self.display_site_name:
            return '%s: %s %s' % (page.site, '- ' * page_depth, page,)
        else:
            return '%s %s' % ('- ' * page_depth, page,)


class PageModelMultipleChoiceField(ModelMultipleChoiceField):
    def __init__(self, display_site_name=True, *args, **kwargs):
        self.display_site_name = display_site_name
        super(PageModelMultipleChoiceField, self).__init__(*args, **kwargs)

    def label_from_instance(self, page):
        page_depth = getattr(page, 'level', page.depth)
        if self.display_site_name:
            return '%s: %s %s' % (page.site, '- ' * page_depth, page,)
        else:
            return '%s %s' % ('- ' * page_depth, page,)


class GenericRelationshipMixin(models.Model):
    """A mixin for adding generic relationship fields to a model."""

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey()

    class Meta:
        abstract = True


class Zone(models.Model):
    name = models.CharField(max_length=31, help_text=_("This must be the exact same string as used in the template "
                                                       "tag. For example: 'home'"))
    description = models.TextField(blank=True)
    number_of_slots = models.IntegerField()
    content_types = models.ManyToManyField(ContentType,
                                           limit_choices_to=(Q(app_label='teasing') & Q(model__endswith='teaser')))

    site = models.ForeignKey(Site)

    class Meta:
        ordering = ['site', 'name']

    def _exclude_pages(self, resultset, page, teaser_model_instance):
        """
        Exclude all teaser that link to our current page

        """
        exclude_args = dict()
        page_fields = []

        if page:
            for field in teaser_model_instance.get_fields():
                if isinstance(field, models.ForeignKey) and field.rel.to == type(cms.models.pagemodel.Page()):
                    page_fields.append(field)

            for field in page_fields:
                exclude_args[field.name] = page

            resultset = resultset.exclude(**exclude_args)

        return resultset

    def _fetch_teaser_baseset(self, teaser_model_class, page=None):
        """
        Get active teasers for that zone / page / page pool config
        exclude teaser linking on the given current page

        """
        teaser_model_instance = teaser_model_class()

        result = teaser_model_class.active.filter(
            Q(placement__zone=self),
            Q(placement__page=None) | Q(placement__page=page),
            Q(placement__page_pool=None) | Q(placement__page_pool__pages=page),
        )

        result = self._exclude_pages(result, page, teaser_model_instance)
        return result

    def _merge_teasers(self, teaser_set1, teaser_set2):
        """
        Filter duplicates and merge two teaser results

        """
        result = teaser_set1
        existing_teaser = {}

        for slot2 in teaser_set2.keys():
            for teaser2 in teaser_set2.get(slot2):
                for slot1 in teaser_set1.keys():
                    if teaser2 in teaser_set1.get(slot1):
                        existing_teaser[str(teaser2.__class__) + "." + str(teaser2.id)] = True

                if not existing_teaser.get(str(teaser2.__class__) + "." + str(teaser2.id)):
                    existing_teaser[str(teaser2.__class__) + "." + str(teaser2.id)] = True
                    result.setdefault(slot2, []).append(teaser2)
            else:
                for teaser in teaser_set2.get(slot2):
                    if not existing_teaser.get(str(teaser.__class__) + "." + str(teaser.id)):
                        existing_teaser[str(teaser.__class__) + "." + str(teaser.id)] = True
                        result.setdefault(slot2, []).append(teaser)

        return result

    def _filter_fixed_slot(self, result, free_slots=[], page=None):
        """
        filter teaser that are not placed on the specified fixed slot
        """
        teasers = {}

        for slot in free_slots:
            try:
                teaser = result.filter(Q(placement__fixed_slot=slot),
                                       Q(placement__page=page) | Q(placement__page_pool__pages=page),
                                       Q(placement__zone=self)).distinct()[0]
                teasers.setdefault(slot, []).append(teaser)
            except IndexError:
                pass

        return teasers

    def _get_page_teaser(self, page, free_slots=[]):
        """
        get only fix or non-fix teaser for current page
        """
        teasers = {}

        for ct in self.content_types.all():
            teaser_model_class = ct.model_class()
            result = self._fetch_teaser_baseset(teaser_model_class, page)

            # fixed slot mode
            if free_slots:
                teasers.update(self._filter_fixed_slot(result, free_slots, page))
            else:
                teaser = result.filter(Q(placement__page=page) | Q(placement__page_pool__pages=page),
                                       placement__fixed_slot=None)
                teasers.setdefault('random', []).extend(teaser)

        return teasers

    def _get_global_teaser(self, free_slots=[]):
        """
        get only global fix or non-fix teasers
        """
        teasers = {}

        for ct in self.content_types.all():
            teaser_model_class = ct.model_class()
            result = self._fetch_teaser_baseset(teaser_model_class)

            # fixed slot mode
            if free_slots:
                teasers.update(self._filter_fixed_slot(result, free_slots))
            else:
                teasers.setdefault('random', []).extend(result.filter(placement__fixed_slot=None))

        return teasers

    def _get_inherit_teaser(self, page, free_slots=[]):
        """
        search all parents to inherit fix or non-fix teasers
        """
        teasers = {}

        if not page:
            return teasers

        inherit_page = page.parent

        for ct in self.content_types.all():
            teaser_model_class = ct.model_class()

            while inherit_page:
                result = self._fetch_teaser_baseset(teaser_model_class, inherit_page)
                result = result.filter(Q(placement__page=inherit_page) | Q(placement__page_pool__pages=inherit_page),
                                       placement__inheritance=True)
                result = self._exclude_pages(result, page, teaser_model_class())

                # fixed slot mode
                if free_slots:
                    result = result.filter(placement__fixed_slot__in=free_slots)

                    for teaser in result:
                        for placement in teaser.placement.all():
                            teasers.setdefault(placement.fixed_slot, []).append(teaser)

                else:
                    result = result.filter(placement__fixed_slot=None)

                    for teaser in result:
                        teasers.setdefault('random', []).append(teaser)

                if len(teasers) > 0:
                    break
                else:
                    inherit_page = inherit_page.parent

        return teasers

    def _check_free_slots(self, teasers):
        """
        check which slots don't have a teaser yet and if we got enough
        random teasers to fill the missing slots
        """
        free_slots = range(1, self.number_of_slots + 1)

        for slot in teasers.keys():
            try:
                del free_slots[slot - 1]
            except IndexError:
                pass
            except TypeError:
                pass

        if teasers.get('random') and len(teasers.get('random')) >= len(free_slots):
            return False
        else:
            return free_slots

    def get_teaserset(self, page=None):
        """
        fill all slots of the zone
        """

        # Since teasers are linked to published page instances only we have to make sure
        # that in draft mode, the published page is used to fetch related teasers.
        # Otherwise the teasers won't be displayed in draft mode.
        # See for reference: https://notch-interactive.atlassian.net/browse/SCF-254
        if page and page.publisher_is_draft and page.publisher_public:
            page = page.publisher_public

        result = []
        teasers = {}
        free_slots = range(1, self.number_of_slots + 1)
        operations = [
            {'name': 'fix page teaser', 'func': lambda free_slots: self._get_page_teaser(page, free_slots)},
            {'name': 'non-fix page teaser', 'func': lambda: self._get_page_teaser(page)},
            {'name': 'inherit fix page teaser', 'func': lambda free_slots: self._get_inherit_teaser(page, free_slots)},
            {'name': 'inherit non-fix page teaser', 'func': lambda: self._get_inherit_teaser(page)},
            {'name': 'global fix teaser', 'func': lambda free_slots: self._get_global_teaser(free_slots)},
            {'name': 'global non-fix teaser', 'func': lambda: self._get_global_teaser()},
        ]

        # call ops as long as there are free slots
        for op in operations:
            try:
                new_teasers = op['func']()
            except TypeError:
                new_teasers = op['func'](free_slots)

            if new_teasers:
                teasers = self._merge_teasers(teasers, new_teasers)
                free_slots = self._check_free_slots(teasers)

            if not free_slots:
                break

        # fill all slots with fix or random teaser
        for slot in range(1, self.number_of_slots + 1):
            if teasers.get(slot):
                index = random.randrange(len(teasers[slot]))
                result.append(teasers[slot][index])
                del teasers[slot][index]
            elif teasers.get('random'):
                index = random.randrange(len(teasers['random']))
                result.append(teasers['random'][index])
                del teasers['random'][index]

        return result

    def __unicode__(self):
        return '%s - %s (%s slots)' % (self.site, self.name, self.number_of_slots,)


class PagePool(models.Model):
    name = models.CharField(max_length=128)
    pages = models.ManyToManyField(Page, blank=True)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        ordering = ['name']


class Placement(GenericRelationshipMixin):
    zone = models.ForeignKey(Zone, null=False)
    page = models.ForeignKey(Page, blank=True, null=True)
    page_pool = models.ForeignKey(PagePool, null=True, blank=True)
    fixed_slot = models.IntegerField(blank=True, null=True)
    publish_from = models.DateTimeField(blank=True, null=True)
    publish_until = models.DateTimeField(blank=True, null=True)
    inheritance = models.BooleanField(default=False)

    def clean(self):
        # check we got a zone
        try:
            self.zone
        except Zone.DoesNotExist:
            raise ValidationError("You need to specify a zone")

        # check for zone index out of range
        if self.fixed_slot is not None and (self.fixed_slot <= 0 or self.fixed_slot > self.zone.number_of_slots):
            raise ValidationError('Slot out of range. Selected zone has %d slots' % self.zone.number_of_slots)

        # selected page and page pool?
        if self.page and self.page_pool:
            raise ValidationError('You cannot select page and page pool at the same time')

        for ct in self.zone.content_types.all():
            teaser = ct.model_class()
            other_teaser = None

            # check if other fix_sloted teaser on that page exists
            if self.page and self.fixed_slot:
                other_teaser = teaser.active.exclude(id=self.object_id).filter(placement__zone=self.zone,
                                                                               placement__page=self.page,
                                                                               placement__fixed_slot=self.fixed_slot)

                # check for overlapping times
                if self.publish_from:
                    other_teaser = other_teaser.filter(placement__zone=self.zone,
                                                       placement__publish_until__gte=self.publish_from,
                                                       placement__page=self.page,
                                                       placement__fixed_slot=self.fixed_slot)

                if self.publish_until:
                    other_teaser = other_teaser.filter(placement__zone=self.zone,
                                                       placement__publish_from__lte=self.publish_until,
                                                       placement__page=self.page,
                                                       placement__fixed_slot=self.fixed_slot)

            # fixed slot in page pool
            elif self.page_pool and self.fixed_slot:
                pool_pages = PagePool.objects.get(pk=self.page_pool.id).pages.all()
                other_teaser = teaser.active.exclude(id=self.object_id).filter(placement__zone=self.zone,
                                                                               placement__page__in=pool_pages,
                                                                               placement__fixed_slot=self.fixed_slot).distinct()

                # check for overlapping times
                if self.publish_from:
                    other_teaser = other_teaser.filter(placement__zone=self.zone,
                                                       placement__publish_until__gte=self.publish_from,
                                                       placement__page__in=pool_pages,
                                                       placement__fixed_slot=self.fixed_slot)

                if self.publish_until:
                    other_teaser = other_teaser.filter(placement__zone=self.zone,
                                                       placement__publish_from__lte=self.publish_until,
                                                       placement__page__in=pool_pages,
                                                       placement__fixed_slot=self.fixed_slot)

            # global fixed slot
            elif self.fixed_slot:
                other_teaser = teaser.active.exclude(id=self.object_id).filter(placement__zone=self.zone,
                                                                               placement__fixed_slot=self.fixed_slot,
                                                                               placement__page=None)

                # check for overlapping times
                if self.publish_from:
                    other_teaser = other_teaser.filter(placement__zone=self.zone,
                                                       placement__publish_until__gte=self.publish_from,
                                                       placement__fixed_slot=self.fixed_slot,
                                                       placement__page=None)

                if self.publish_until:
                    other_teaser = other_teaser.filter(placement__zone=self.zone,
                                                       placement__publish_from__lte=self.publish_until,
                                                       placement__fixed_slot=self.fixed_slot,
                                                       placement__page=None)

            if other_teaser:
                msg = "There is already another teaser '%s' on slot %d" % (
                ','.join(map(str, other_teaser)), self.fixed_slot)

                if self.page:
                    msg += " on page '%s'" % self.page
                if self.page_pool:
                    other_pages = {}
                    msg += " on pages"

                    for teaser in other_teaser:
                        for placement in teaser.placement.all():
                            if placement.fixed_slot == self.fixed_slot:
                                other_pages[placement.page] = True

                    for page in pool_pages:
                        if other_pages.get(page):
                            msg += " '%s'" % page

                if self.publish_from or self.publish_until:
                    msg += " in the same time slot"

                raise ValidationError(msg)


class TeaserActiveManager(models.Manager):
    def get_queryset(self):
        return super(TeaserActiveManager, self).get_queryset().filter(is_active=True).filter(
            Q(placement__publish_from__lte=now()) | Q(placement__publish_from=None),
            Q(placement__publish_until__gte=now()) | Q(placement__publish_until=None)
        )


class CmsPageLinkMixin(models.Model):
    page = PageField(blank=True, null=True)

    class Meta:
        abstract = True

    def get_target_url(self):
        if self.page:
            domain = self.page.site.domain
            return 'http://%s%s' % (domain, self.page.get_absolute_url(),)
        else:
            return None


class BaseTeaser(models.Model):
    is_active = models.BooleanField(default=False)
    placement = generic.GenericRelation(Placement)

    objects = models.Manager()
    active = TeaserActiveManager()

    def get_fields(self):
        return self._meta.fields

    class Meta:
        abstract = True


class TestTeaser(BaseTeaser, CmsPageLinkMixin):
    title = models.CharField(max_length=100)
