from django.contrib import admin
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType

from djangocms_teasing.forms import PlacementAdminForm, PagePoolAdminForm
from djangocms_teasing.models import Zone, Placement, PagePool


class PlacementInline(generic.GenericTabularInline):
    model = Placement
    form = PlacementAdminForm
    extra = 1

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'zone':
            teaser_type = ContentType.objects.get_for_model(self.parent_model)
            kwargs['queryset'] = Zone.objects.filter(content_types=teaser_type)
        return super(PlacementInline, self).formfield_for_foreignkey(db_field, request, **kwargs)


class ZoneAdmin(admin.ModelAdmin):
    model = Zone
    list_display = ['name', 'description', 'get_content_types']
    filter_horizontal = ["content_types"]
    fields = ["site", "name", "content_types", "number_of_slots", "description"]

    def get_content_types(self, obj):
        return ', '.join(list("({0}, {1})".format(ct.app_label, ct.model) for ct in obj.content_types.all()))


admin.site.register(Zone, ZoneAdmin)


class PagePoolAdmin(admin.ModelAdmin):
    model = PagePool
    form = PagePoolAdminForm
    list_display = ['name', 'description']


admin.site.register(PagePool, PagePoolAdmin)
