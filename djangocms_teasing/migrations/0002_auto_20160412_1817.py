# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('djangocms_teasing', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='zone',
            name='name',
            field=models.CharField(
                help_text="This must be the exact same string as used in the template tag. For example: 'home'",
                max_length=31),
        ),
    ]
