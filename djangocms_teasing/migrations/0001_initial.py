# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import cms.models.fields
from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('cms', '__latest__'),
        ('contenttypes', '__latest__'),
        ('sites', '__latest__'),
    ]

    operations = [
        migrations.CreateModel(
            name='PagePool',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('description', models.TextField(blank=True)),
                ('pages', models.ManyToManyField(to='cms.Page', blank=True)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Placement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField()),
                ('fixed_slot', models.IntegerField(null=True, blank=True)),
                ('publish_from', models.DateTimeField(null=True, blank=True)),
                ('publish_until', models.DateTimeField(null=True, blank=True)),
                ('inheritance', models.BooleanField(default=False)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('page', models.ForeignKey(blank=True, to='cms.Page', null=True)),
                ('page_pool', models.ForeignKey(blank=True, to='djangocms_teasing.PagePool', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TestTeaser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=False)),
                ('title', models.CharField(max_length=100)),
                ('page', cms.models.fields.PageField(blank=True, to='cms.Page', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Zone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=31)),
                ('description', models.TextField(blank=True)),
                ('number_of_slots', models.IntegerField()),
                ('content_types', models.ManyToManyField(to='contenttypes.ContentType')),
                ('site', models.ForeignKey(to='sites.Site')),
            ],
            options={
                'ordering': ['site', 'name'],
            },
        ),
        migrations.AddField(
            model_name='placement',
            name='zone',
            field=models.ForeignKey(to='djangocms_teasing.Zone'),
        ),
    ]
