from cms.utils.page_resolver import get_page_queryset
from django import forms
from django.forms.models import ModelForm

from .models import Placement, PageModelChoiceField, PageModelMultipleChoiceField, PagePool


class PlacementAdminForm(ModelForm):
    page = PageModelChoiceField(queryset=get_page_queryset(), required=False, label='Place on this Page only')

    class Meta:
        model = Placement
        fields = '__all__'


class PagePoolAdminForm(ModelForm):
    pages = PageModelMultipleChoiceField(queryset=get_page_queryset(), required=True,
                                         widget=forms.SelectMultiple(attrs={'size': 20}))

    class Meta:
        model = PagePool
        fields = '__all__'
