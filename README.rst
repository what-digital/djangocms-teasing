##################
Django CMS Teasing
##################

A teasing system used with Django CMS.

Warning
=======

This packages is intended for internal use at Notch Interactive.
Stuff may break from one version to another and backwards compatibility may not be guaranteed.
Docs and tests are missing too.

We may eventually publish an official release at the Cheese Shop.
Until then use this piece of software at your own peril.