# -*- coding: utf-8 -*-
import codecs
import re
from os import path
from setuptools import find_packages

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def read_file_content(*parts):
    file_path = path.join(path.dirname(__file__), *parts)
    return codecs.open(file_path, encoding='utf-8').read()


def find_version(*parts):
    version_file = read_file_content(*parts)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError('Unable to find version string.')


CLASSIFIERS = [
    'Development Status :: 4 - Beta',
    'Environment :: Web Environment',
    'Framework :: Django',
    'Intended Audience :: Developers',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Programming Language :: Python :: 2.6',
    'Programming Language :: Python :: 2.7',
    'Topic :: Software Development',
    ]

setup(
    name='djangocms_teasing',
    version=find_version('djangocms_teasing', '__init__.py'),
    author='Notch Interactive',
    author_email='office@notch-interactive.com',
    packages=find_packages(),
    include_package_data=True,
    license='LICENSE',
    description='A teasing system used with Django CMS.',
    long_description=read_file_content('README.rst'),
    install_requires=[
        'Django>=1.4.1',
        'django-cms',
        ],
    classifiers=CLASSIFIERS,
)
